var gulp            = require('gulp'),
    config          = require('../config'),
    imagemin        = require('gulp-imagemin');



gulp.task('images', function(){
    return gulp.src(config.images.src + '*')
    .pipe(imagemin([
        imagemin.gifsicle({interlaced: true}),
        imagemin.jpegtran({progressive: true}),
        imagemin.optipng({optimizationLevel: 5}),
    ]))
    .pipe(gulp.dest(config.images.dist))
});
